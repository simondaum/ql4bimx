﻿using QL4BIMinterpreter.QL4BIM;

namespace QL4BIMinterpreter
{
    public interface IReportWriter
    {

        void SetFilePathFromModelFile(string modelPath);

        string FilePath { get; }

        void AddEntry(SetSymbol setSymbol, string opName);
        void WriteReport();
        void AddEntry(RelationSymbol relSymbol, string opName);
    }
}