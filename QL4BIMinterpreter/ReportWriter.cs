﻿using Microsoft.Practices.ObjectBuilder2;
using Newtonsoft.Json.Linq;
using QL4BIMinterpreter.QL4BIM;
using QL4BIMspatial;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL4BIMinterpreter
{
    public class ReportWriter : IReportWriter
    {
        const string newlineremover = "%newline%";

        public string FilePath { get; set; }

        readonly IList<ReportEntry> reportEntries;

        public ReportWriter()
        {
            reportEntries = new List<ReportEntry>();
        }

        public void AddEntry(SetSymbol setSymbol, string opName)
        {
            var entry = new ReportEntry();
            entry.Variable = setSymbol.Value;
            entry.Operator = opName;

            var entities = setSymbol.Entites;

            if (entities.Length > 100)
            {
                entry.OnlyTopItems = true;
                entities = entities.Take(100).ToArray();
            }


            entry.EntityIds = entities.Select(e => EntityToId(e)).ToList();

            reportEntries.Add(entry);
        }

        private string EntityToId(QLEntity entity)
        {
            if (string.IsNullOrEmpty(entity.GlobalId))
                return "IID_" + entity.Id + newlineremover;
            else
                return "GID_" + entity.GlobalId + newlineremover;
        }

        public void AddEntry(RelationSymbol relSymbol, string opName)
        {
            var entry = new ReportEntry();
            entry.Variable = relSymbol.Value;
            entry.Operator = opName;

            var tuples = relSymbol.Tuples.ToArray();

            if (tuples.Length > 100)
            {
                entry.OnlyTopItems = true;
                tuples = tuples.Take(100).ToArray();
            }


            entry.EntityIds = tuples.Select(tuple =>
            {
                return tuple.Select(e => EntityToId(e)).ToList();
            }).ToList();

            reportEntries.Add(entry);
        }

        public void WriteReport()
        {
            var report = new ReportMeta(reportEntries);
            JObject o = (JObject)JToken.FromObject(report);

            var jsonout = o.ToString();
            jsonout = jsonout.Replace(newlineremover + "\"," +  Environment.NewLine, "\",");
            jsonout = jsonout.Replace(",        ", ", ");
            jsonout = jsonout.Replace(",   ", ", ");
            jsonout = jsonout.Replace("[" + Environment.NewLine, "[");
            jsonout = jsonout.Replace("]," + Environment.NewLine, "], ");
            jsonout = jsonout.Replace(Environment.NewLine + "        ", "");
            jsonout = jsonout.Replace("        [          ", "[");
            jsonout = jsonout.Replace("[        ", "[");




            jsonout = jsonout.Replace(newlineremover, "");

            File.AppendAllText(FilePath, jsonout);
        }

        public void SetFilePathFromModelFile(string modelPath)
        {
            var now = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            FilePath = Path.Combine(Path.GetDirectoryName(modelPath), "report_" + now + ".json");
        }
    }

    public class ReportMeta
    {
        public DateTime DateTime { get; set; }

        public IList<ReportEntry> ReportEntries { get; set; }


        public ReportMeta(IList<ReportEntry> reportEntries)
        {
            DateTime = DateTime.Now;
            ReportEntries = reportEntries;
        }
    }

    public class ReportEntry
    {
        public string Operator { get; set; }
        public string Variable { get; set; }

        public bool OnlyTopItems { get; set; }

        public dynamic EntityIds { get; set; }
    }

    public class EntityId {
        public string Guid { get; set; }
        public string InternalId { get; set; }

        public EntityId(string guid, string internalid)
        {
            Guid = guid;
            InternalId = internalid;
        }
    }
}
